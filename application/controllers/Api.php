<?php 
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
/**
 * 
 */
 class Api extends CI_Controller {
 	
 	function __construct(){
 		parent::__construct();
 		$this->load->model('api_mahasiswa');
 	}

 	function index(){
 		$this->send_result_if_access_index_function();
 	}

 	function send_result_if_access_index_function(){
 		$result = array(
 			'status' => 'Hello world!',
 			'message' => 'Apa yang kamu inginkan ?' );
		$this->output->set_output(
			json_encode(
				array('result' => $result)
			)
		);
 	}

 	function mahasiswa(){
 		$method = $this->input->get('method');
 		if ($method === 'get') {
 			$this->api_mahasiswa->get_mahasiswa();

 		} else if ($method === 'post'){
 			$this->api_mahasiswa->post_mahasiswa();

 		} else if ($method === 'update') {
 			$this->api_mahasiswa->update_mahasiswa();

 		} else if ($method === 'delete') {
 			$this->api_mahasiswa->delete_mahasiswa();

 		} else {
 			$this->send_result_if_access_index_function();
 		}
 	}

 } 
 ?>